package be.kdg.java2.randomnumbers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RandomnumbersApplication {

    public static void main(String[] args) {
        SpringApplication.run(RandomnumbersApplication.class, args);
    }

}
