package be.kdg.java2.randomnumbers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

@Controller
public class RandomController {
    private static final Logger logger = LoggerFactory.getLogger(RandomController.class);
    /*@GetMapping("/random")
    public String getRandomNumber(@RequestParam Optional<Integer> max, Model model){
        int maxValue = max.orElse(100);
        model.addAttribute("randomnumber", new Random().nextInt(maxValue));
        return "random";
    }*/

    @GetMapping("/random")
    public String getRandomNumber(@RequestParam Optional<List<Integer>> max,
                                  @RequestParam(required = false) boolean even, Model model) {
        List<Integer> values = max.orElse(new ArrayList<>());
        logger.debug("Number of values: " + values.size());
        int low = 0;
        int high = 100;
        if (values.size() == 1) high = values.get(0);
        if (values.size() == 2) {
            low = values.get(0);
            high = values.get(1);
        }
        if (even) {
            low /= 2;
            high /= 2;
            model.addAttribute("randomnumber",
                    2*(new Random().nextInt(high - low) + low));
        } else {
            model.addAttribute("randomnumber",
                    new Random().nextInt(high - low) + low);
        }
        return "random";
    }
}
